# 1. 新增SPU的界面设计

**视图文件名：**`SpuAddNewStep1View.vue`

**路由路径：**`/sys-admin/product/spu-add-new1`

![](images/image-20220915104032978.png)

**视图文件名：**`SpuAddNewStep2View.vue`

**路由路径：**`/sys-admin/product/spu-add-new2`

![](images/image-20220915104053183.png)

**视图文件名：**`SpuAddNewStep3View.vue`

**路由路径：**`/sys-admin/product/spu-add-new3`

![](images/image-20220915104105055.png)

**视图文件名：**`SpuAddNewStep4View.vue`

**路由路径：**`/sys-admin/product/spu-add-new4`

![](images/image-20220915104116558.png)

在前端界面的设计中，HTML本身并没有**富文本编辑器**的控件，通常，都是使用第三方的。

第三方的富文本编辑器有许多种，以上演示图中使用的是`wangeditor`，在使用之前，需要先安装：

```
npm i wangeditor -S
```

然后，在`main.js`中导入，并声明为Vue对象的属性：

```javascript
import wangEditor from 'wangeditor';

Vue.prototype.wangEditor = wangEditor;
```

在设计视图时，只需要使用某个标签用于后续`wangEditor`向其它插入源代码，以显示富编辑器即可，例如：

```html
<div id="wangEditor"></div>
```

并且，在页面刚刚加载完成时，应该对`wangEditor`进行初始化：

```javascript
<template>
  <div>
    <h1>新增SPU第4步</h1>
    <div id="wangEditor"></div>
  </div>
</template>

<script>
export default {
  data() {
    return {
      editor: {}
    }
  },
  methods: {
    initWangEditor() {
      this.editor = new this.wangEditor('#wangEditor');
      // this.editor.config.zIndex = 1;
      this.editor.create();
    }
  },
  mounted() {
    this.initWangEditor();
  }
}
</script>
```

# 增加SPU之前的检查

在增加SPU之前，应该对输入（或选择等）的数据进行检查，例如：检查类别信息，则需要服务器端提供“根据类别id获取类别详情”的接口！

在`CategoryMapper`接口中已经实现了`CategoryStandardVO getStandardById(Long id);`。

在`ICategoryService`接口中添加：

```java
/**
 * 根据id获取类别的标准信息
 *
 * @param id 类别id
 * @return 返回匹配的类别的标准信息，如果没有匹配的数据，将返回null
 */
CategoryStandardVO getStandardById(Long id);
```

在`CategoryServiceImpl`中实现：

```java
@Override
public CategoryStandardVO getStandardById(Long id) {
    log.debug("开始处理【根据id查询类别详情】的业务");
    CategoryStandardVO category = categoryMapper.getStandardById(id);
    if (category == null) {
        // 是：此id对应的数据不存在，则抛出异常(ERR_NOT_FOUND)
        String message = "查询类别详情失败，尝试访问的数据不存在！";
        log.warn(message);
        throw new ServiceException(ServiceCode.ERR_NOT_FOUND, message);
    }
    return category;
}
```

在`CategoryServiceTests`中测试：

```java
@Test
public void testGetStandardById() {
    Long id = 2L;
    try {
        CategoryStandardVO category = service.getStandardById(id);
        log.debug("根据id={}查询完成，查询结果={}", id, category);
    } catch (ServiceException e) {
        log.debug("serviceCode : " + e.getServiceCode());
        log.debug("message : " + e.getMessage());
    }
}
```

在`CategoryController`中处理请求：

```java
// http://localhost:9080/categories/3
@ApiOperation("根据id查询类别详情")
@ApiOperationSupport(order = 400)
@ApiImplicitParam(name = "id", value = "类别id", required = true, dataType = "long")
@GetMapping("/{id:[0-9]+}")
public JsonResult<CategoryStandardVO> getStandardById(@PathVariable Long id) {
    log.debug("开始处理【根据id查询类别详情】的请求：id={}", id);
    CategoryStandardVO category = categoryService.getStandardById(id);
    return JsonResult.ok(category);
}
```

完成后，重启项目，可以通过在线API文档进行测试访问。

同理，还需要**检查品牌信息**，则需要服务器端提供“根据品牌id获取品牌详情”的接口！



