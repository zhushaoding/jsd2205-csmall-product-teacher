# Mybatis的#{}与${}占位符

在使用Mybatis配置SQL语句时，SQL语句中的参数可以使用`#{}`格式的占位符，例如：

```xml
<!-- AlbumStandardVO getStandardById(Long id); -->
<select id="getStandardById" resultMap="StandardResultMap">
    SELECT
        <include refid="StandardQueryFields"/>
    FROM
        pms_album
    WHERE
        id=#{id}
</select>
```

其实，还可以使用`${}`占位符！

以上配置，使用`#{}`或`${}`格式的占位符均可正常通过测试！

另外，再使用一段代码进行测试：

```xml
<!-- int countByName(String name); -->
<select id="countByName" resultType="int">
    SELECT count(*) FROM pms_album WHERE name=#{name}
</select>
```

以上代码，使用`#{}`格式的占位符可以正常通过测试，但是，使用`${}`格式的占位符则会出现错误：

```
Cause: java.sql.SQLSyntaxErrorException: Unknown column '小米13的相册' in 'where clause'
; bad SQL grammar []; nested exception is java.sql.SQLSyntaxErrorException: Unknown column '小米13的相册' in 'where clause'
```

通过错误信息，可以看到，SQL语句中将测试参数 `小米13的相册` 作为了列名（字段名）！

其实，在SQL语句中，**除了关键词、数值、运算符、函数名等非常固定的内容以外，所有内容都会被视为某个名字，例如：表名、字段名或其它自定义的名称（例如索引名等）**，比较典型的应用，例如：

```mysql
update bank_account set money = money + 1000 where id = 1;
```

以上SQL语句的意思大致是：将id=1的账户的余额在现有基础之上增加1000。

可以看到，在以上`set money = money + 1000`部分的代码片段中，等于号右侧的 `money` 会被视为“字段名”！

为了避免MySQL把 `小米13的相册` 视为字段名，必须在 `小米13的相册` 的两侧添加一对单引号，例如：

```mysql
SELECT count(*) FROM pms_album WHERE name='${name}'
```

或者，在测试数据的两侧添加一对单引号也是可以的：

```java
String name = "'小米13的相册'";
```

所以，**在SQL语句中，只有将值使用一对双引号框住（数值除外），才会被认为是“值”**！

```
------------------------- 分隔线 -------------------------
```

在MySQL处理SQL语句时，会经过**词法分析、语义分析，然后再执行编译，最终执行**！

在Mybatis中，使用`#{}`格式的占位符，在底层实现时，会使用**预编译**（在编译时与参数值无关）的处理机制，值将会在执行时再代入！由于采用了预编译的做法，所有值都不用考虑数据类型的问题，例如，不需要给字符串类型的值添加一对单引号，并且，预编译是安全的，**不会出现SQL注入问题**！这种做法中，`#{}`占位符只能表示SQL语句中的某个值，而不能表示其它内容！

使用`${}`格式的占位符，在底层实现时，会**先将值拼接到原SQL语句中，然后再执行编译的相关流程**！由于不是预编译的，字符串、日期等类型的值需要添加一对单引号，否则，将被视为字段名、运算表达式等，由于是先代入值再执行编译相关流程，所以，代入的值是可以改变语义的，**是存在SQL注入风险的**！这种做法中，`${}`占位符可以表示SQL语句中的任何片段，只需要保证拼接起来后可以通过编译即可！

附：SQL注入演示：
```mysql
select * from user where username='xxx' and password='xxxxxxxxxxxx';
--   												  1' or 'a'='a


select * from user where username='xxx' and password='1' or '1'='1';
														 or true
```