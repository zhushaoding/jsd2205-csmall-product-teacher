# 1. Knife4j框架

Knife4j是一款基于Swagger 2的在线API文档框架。

在Spring Boot中，使用此框架时，需要：

- 添加依赖
- 在配置文件（`application.properties`）中开启增强模式
- 编写配置类（代码相对固定，建议CV）

关于依赖的代码：

```xml
<!-- Knife4j Spring Boot：在线API -->
<dependency>
    <groupId>com.github.xiaoymin</groupId>
    <artifactId>knife4j-spring-boot-starter</artifactId>
    <version>2.0.9</version>
</dependency>
```

关于开启增强模式，在`application.properties`中添加：

```properties
# 开启Knife4j的增强模式
knife4j.enable=true
```

关于配置类，在项目的根包下创建`config.Knife4jConfiguration`，代码如下：

**注意：请检查`basePackage`属性的值！**

```java
package cn.tedu.csmall.product.config;

import com.github.xiaoymin.knife4j.spring.extension.OpenApiExtensionResolver;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2WebMvc;

/**
 * Knife4j配置类
 *
 * @author java@tedu.cn
 * @version 0.0.1
 */
@Slf4j
@Configuration
@EnableSwagger2WebMvc
public class Knife4jConfiguration {

    /**
     * 【重要】指定Controller包路径
     */
    private String basePackage = "cn.tedu.csmall.product.controller";
    /**
     * 分组名称
     */
    private String groupName = "product";
    /**
     * 主机名
     */
    private String host = "http://java.tedu.cn";
    /**
     * 标题
     */
    private String title = "酷鲨商城在线API文档--商品管理";
    /**
     * 简介
     */
    private String description = "酷鲨商城在线API文档--商品管理";
    /**
     * 服务条款URL
     */
    private String termsOfServiceUrl = "http://www.apache.org/licenses/LICENSE-2.0";
    /**
     * 联系人
     */
    private String contactName = "Java教学研发部";
    /**
     * 联系网址
     */
    private String contactUrl = "http://java.tedu.cn";
    /**
     * 联系邮箱
     */
    private String contactEmail = "java@tedu.cn";
    /**
     * 版本号
     */
    private String version = "1.0.0";

    @Autowired
    private OpenApiExtensionResolver openApiExtensionResolver;

    public Knife4jConfiguration() {
        log.debug("加载配置类：Knife4jConfiguration");
    }

    @Bean
    public Docket docket() {
        String groupName = "1.0.0";
        Docket docket = new Docket(DocumentationType.SWAGGER_2)
                .host(host)
                .apiInfo(apiInfo())
                .groupName(groupName)
                .select()
                .apis(RequestHandlerSelectors.basePackage(basePackage))
                .paths(PathSelectors.any())
                .build()
                .extensions(openApiExtensionResolver.buildExtensions(groupName));
        return docket;
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title(title)
                .description(description)
                .termsOfServiceUrl(termsOfServiceUrl)
                .contact(new Contact(contactName, contactUrl, contactEmail))
                .version(version)
                .build();
    }

}
```

**注意：以上代码适用于Spring Boot 2.6以下（不含2.6）版本！**

完成后，重启项目，打开浏览器，通过 http://localhost:9080/doc.html 即可访问Knife4j的API文档。

关于Knife4j框架，还提供了一系列的注解，便于实现API文档的显示，包括：

- `@Api`：添加在控制器类上，配置其`tags`属性，用于指定模块名称，在指定的模块名称，可以使用数字编号作为名称的前缀，则多个管理模块将按照编号顺序来显示，例如：

  - ```java
    @RestController
    @RequestMapping("/albums")
    @Api(tags = "03. 相册管理模块")
    public class AlbumController {
    
        @GetMapping("/test")
        public void test() {}
    
    }
    ```

- `@ApiOperation`：添加在控制器类中处理请求的方法上，配置其`value`属性，用于指定业务接口名称，例如：

  - ```java
    @ApiOperation("删除品牌")
    @PostMapping("/delete")
    public String delete(Long id) {
    }
    ```

- `@ApiOperationSupport`：添加在控制器类中处理请求的方法上，配置其`order`属性，用于指定业务接口的排序编号，最终，同一个模块中的多个业务接口将按此编号升序排列，例如：

  - ```java
    @ApiOperation("删除品牌")
    @ApiOperationSupport(order = 200)
    @PostMapping("/delete")
    public String delete(Long id) {
    }
    ```

- `@ApiModelProperty`：添加在POJO类的属性上，配置其`value`属性，用于指定请求参数的名称（说明），配置其`required`属性，用于指定“是否必须提交此请求参数”（仅用于显示，不具备检查功能），配置其`example`属性，用于指定“示例例”，例如：

  - ```java
    @Data
    public class BrandAddNewDTO implements Serializable {
    
        /**
         * 是否启用，1=启用，0=未启用
         */
        @ApiModelProperty(value = "是否启用，1=启用，0=未启用", example = "1", required = true)
        private Integer enable;
    
    }
    ```

- `@ApiImplicitParam`：添加在控制器类中处理请求的方法上，配置其`name`属性，指定方法的参数的变量名，配置其`value`属性，指定此参数的说明，配置其`required`属性，指定此参数“是否必须提交”，配置其`dataType`属性，指定此参数的数据类型，例如：

  ```java
  @ApiOperation("删除品牌")
  @ApiOperationSupport(order = 200)
  @ApiImplicitParam(name = "id", value = "品牌id", required = true, dataType = "long")
  @PostMapping("/delete")
  public String delete(Long id) {
  }
  ```

- `@ApiImplicitParams`：添加在控制器类中处理请求的方法上，当有多个参数需要配置时，使用此注解，且此注解的值是`@ApiImplicitParam`的数组，例如：

  - ```java
    @ApiOperation("删除品牌")
    @ApiOperationSupport(order = 200)
    @ApiImplicitParams({
        @ApiImplicitParam(name = "id", value = "品牌id", 
                          required = true, dataType = "long")
    })
    @PostMapping("/delete")
    public String delete(Long id) {
    }
    ```

- `@ApiIgnore`：添加在处理请求的方法的参数上，当某个参数不需要显示在API文档中，则需要在参数上添加此注解，例如`HttpServletRequest`、`HttpSession`等，例如：

  ```java
  @ApiOperation("删除品牌")
  @ApiOperationSupport(order = 200)
  @ApiImplicitParam(name = "id", value = "品牌id", required = true, dataType = "long")
  @PostMapping("/delete")
  public String delete(Long id, @ApiIgnore HttpSession session) {
  }
  ```

# 2. Spring MVC与RESTful

在Spring MVC框架中，接收请求参数的做法有：

- 将各请求参数声明为处理请求的方法的参数
- 将各请求参数封装到自定义的POJO类型中，并使用POJO类型作为处理请求的方法的参数
- 【见下文】在配置请求路径时使用占位符，并通过`@PathVariable`注解来接收请求参数的值

RESTful是一种设计软件的风格，其典型特征包括：将具有“唯一性”的请求参数值作为URL的一部分，例如：

```
https://blog.csdn.net/a6244353135_/article/details/1242675432835
```

Spring MVC框架很好的支持了RESTful，在使用`@RequestMapping`系列注解配置请求路径时，可以使用`{名称}`作为占位符来接收请求，例如配置为：

```
@PostMapping("/{id}/delete")
```

则以上路径中`{id}`可以是任何值，均能匹配到以上路径！

在处理请求的方法上，仍使用`Long id`来接收URL中的占位符对应的值，并且，此参数需要添加`@PathVariable`注解，例如：

```java
// http://localhost:9080/brands/3/delete
@PostMapping("/{id}/delete")
public String delete(@PathVariable Long id) {
}
```

在使用`@PathVariable`注解时，如果请求参数的名称与占位符不一致时，可以通过注解参数进行配置，例如：

```java
// http://localhost:9080/brands/3/delete
@PostMapping("/{id}/delete")
public String delete(@PathVariable("id") Long brandId) {
}
```

另外，在`{}`占位符中，可以在自定义名称的右侧添加冒号（`:`），并在冒号的右侧添加正则表达式，以实现按需匹配，例如：

```java
// http://localhost:9080/brands/3/delete
@PostMapping("/{id:[0-9]+}/delete")
public String delete(@PathVariable Long id) {
}
```

在同一个项目中，多个使用了占位符、且正则表达式不冲突的URL，是允许共存的！例如：

```java
// http://localhost:9080/brands/3/delete
@PostMapping("/{id:[0-9]+}/delete")
public String delete(@PathVariable Long id) {
}

// http://localhost:9080/brands/hello/delete
@PostMapping("/{id:[a-zA-Z]+}/delete")
public String delete(@PathVariable String id) {
}
```

另外，使用了占位符的URL，与不使用占位符的URL，也是允许共存的，例如：

```java
// http://localhost:9080/brands/3/delete
@PostMapping("/{id:[0-9]+}/delete")
public String delete(@PathVariable Long id) {
}

// http://localhost:9080/brands/hello/delete
@PostMapping("/{name:[a-zA-Z]+}/delete")
public String delete(@PathVariable String name) {
}

// http://localhost:9080/brands/test/delete
@PostMapping("/test/delete")
public String delete() {
}
```

在RESTful的设计风格中，如果没有更好的选择，在设计URL时，可以：

- `/数据类型的复数`：表示获取某类型的数据的列表
  - 例如：`/brands`表示获取品牌列表
- `/数据类型的复数/{id}`：表示获取某类型的id=?的数据
  - 例如：`/brands/{id}`，实际请求路径可能是`/brands/1`，则表示获取id=1的品牌数据
- `/数据类型的复数/{id}/命令`：表示针对某类型的id=?的数据进行某操作
  - 例如：`/brands/{id}/delete`，实际请求路径可能是`/brands/1/delete`，则表示删除id=1的品牌数据
- `/数据类型的复数/{id}/属性/命令`：表示针对某类型的id=?的数据的某属性进行某操作
- 其它

另外，RESTful思想**建议**针对不同的需求，使用不同的请求方式，如下：

- `GET`：获取数据
- `POST`：增加数据
- `PUT`：修改数据
- `DELETE`：删除数据

# 3. 关于响应结果

控制器处理完请求后，向客户端进行响应时，推荐使用JSON格式的响应数据，并且，此JSON格式的数据中至少应该包括：

- 业务状态码
- 提示信息

在Spring MVC框架中，当需要向客户端响应JSON格式的结果时，需要：

- 当前处理请求的方法必须是“响应正文”的
  - 在处理请求的方法或控制器类上使用`@ResponseBody`，或控制器类上使用了`@RestController`，就是“响应正文”的
- 在项目中添加`jackson-databind`的依赖
  - 包含在`spring-boot-starter-web`依赖项中
- 开启注解驱动
  - 使用注解模式的Spring MVC项目（包括Spring Boot）均默认开启
- 使用自定义的类型作为处理请求的方法的返回值类型，并且，此类中应该包含响应的JSON中的各属性

则在项目的根包下创建`web.JsonResult`类：

```java
package cn.tedu.csmall.product.web;

import cn.tedu.csmall.product.ex.ServiceCode;
import cn.tedu.csmall.product.ex.ServiceException;
import lombok.Data;

import java.io.Serializable;

@Data
public class JsonResult<T> implements Serializable {

    /**
     * 业务状态码
     */
    private Integer state;
    /**
     * 错误时的提示消息
     */
    private String message;
    /**
     * 成功时响应的数据
     */
    private T data;

    public JsonResult() {
    }

    private JsonResult(Integer state, String message, T data) {
        this.state = state;
        this.message = message;
        this.data = data;
    }

    public static JsonResult<Void> ok() {
        return ok(null);
    }

    public static <T> JsonResult<T> ok(T data) {
        return new JsonResult(ServiceCode.OK.getValue(), null, data);
    }

    public static JsonResult<Void> fail(ServiceException e) {
        return fail(e.getServiceCode().getValue(), e.getMessage());
    }

    public static JsonResult<Void> fail(Integer state, String message) {
        return new JsonResult(state, message, null);
    }

}
```

然后，在处理请求的方法中，使用`JsonResult`作为返回值类型，并返回此类型的结果：

```java
@PostMapping("/{id:[0-9]+}/enable")
public JsonResult<Void> setEnable(@PathVariable Long id) {
    log.debug("即将处理【启用品牌】的请求，id={}", id);

    try {
        brandService.setEnable(id);
        return JsonResult.ok();
    } catch (ServiceException e) {
        return JsonResult.fail(e);
    }
}
```

# 4. 关于处理异常

在Java语言中，异常的体系结构大致是：

```
Throwable
-- Error
-- -- OutOfMemoryError（OOM）
-- Exception
-- -- IOException
-- -- RuntimeException
-- -- -- NullPointerException（NPE）
-- -- -- ClassCastException
-- -- -- IndexOutOfBoundsException
-- -- -- -- ArrayIndexOutOfBoundsException
-- -- -- -- StringIndexOutOfBoundsException
```

如果调用的方法抛出了“非`RuntimeException`”，则必须：

- 当前方法声明抛出此异常
- 使用`try...catch`代码块包裹此方法的调整代码
  - 真正意义上的“处理了异常”

关于“处理异常”，需要明确的告诉用户“这次操作失败了，失败的原因是XXXXX，你可以通过XXXXX再次尝试，并避免出现此类错误”！

所以，在整个项目，只有Controller才是适合且必须处理异常的组件，因为它可以将错误的描述文本响应到客户端去，而项目中的其它组件（例如Service等）不适合且不应该处理异常，因为它们不可以直接与客户端进行交互，且如果它们处理了异常，则Controller在调用时将无法知道曾经出现过异常，更加无法处理！