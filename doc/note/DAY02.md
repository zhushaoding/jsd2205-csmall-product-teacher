# 1. 关于CSMall项目

CSMall项目：酷鲨商城，是一个定位为自营性质的电商平台项目。

CSMall Product项目：是整个项目的一部分，主要处理后台商品的数据管理。

商品相关的数据主要有：

- 品牌
- 类别
- 图片与相册
- 属性与属性模板
- SPU与SKU

- 以上数据的关联数据

# 2. 创建项目

在IntelliJ IDEA中，使用Spring Boot项目的创建向导来创建项目，相关参数：

- 项目名称：`jsd2205-csmall-product-teacher`
- `Group`：`cn.tedu`
- `Artifact`：`csmall-product`
- `Package`：`cn.tedu.csmall.product`
- Java版本：`8`

在创建过程中，可以无视Spring Boot版本，且可以不勾选任何依赖项。

当创建成功后，在`pom.xml`中将版本指定为`2.5.9`。

# 3. 数据库与数据表

在终端下，登录MySQL控制台，创建`mall_pms`数据库：

```mysql
CREATE DATABASE mall_pms;
```

接下来，在IntelliJ IDEA打开项目，并配置Database面板，连接到`mall_pms`数据库，并通过`mall_pms.sql`（老师下发的文件）中的代码来创建所需的数据表（将`mall_pms.sql`中的所有代码全部复制到Database面板的Console中，全选并执行）。

至此，本项目所需的数据库和数据表创建完成！

关于配置Database面板的视频教程：http://doc.canglaoshi.org/doc/idea_database/index.html

# 作业

编写以下需求对应的SQL语句（使用记事本保存）：

- 向`pms_brand`表中插入数据
- 根据id删除`pms_brand`表中的某1条数据
- 根据若干个id批量删除`pms_brand`表中的数据
  - 如果没有足够多的测试数据，可以事先添加
- 根据id修改`pms_brand`表中的`name`字段的值
- 统计`pms_brand`表中的数据的数量
- 根据`name`查询`pms_brand`表中的数据
- 根据`id`查询`pms_brand`表中的数据
- 查询`pms_brand`表中所有的数据

















