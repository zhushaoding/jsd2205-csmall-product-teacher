# 1. Spring Validation（续）

默认情况下，Spring Validation框架会在检查所有的请求参数后再提示可能的失败，即“检查到某个错误时并不会直接中止，而是继续检查”，如果需要实现“快速失败”（即：检查到某个错误时直接视为失败，不会继续后续的检查），需要在配置类中使用`@Bean`方法创建并配置`Validator`对象！

则在项目的根包下创建`config.ValidationConfiguration`类，在此配置类中创建并配置`Validator`：

```java
package cn.tedu.csmall.product.config;

import lombok.extern.slf4j.Slf4j;
import org.hibernate.validator.HibernateValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.validation.Validation;

/**
 * Validation的配置类
 *
 * @author java@tedu.cn
 * @version 0.0.1
 */
@Slf4j
@Configuration
public class ValidationConfiguration {

    public ValidationConfiguration() {
        log.info("加载配置类：ValidationConfiguration");
    }

    @Bean
    public javax.validation.Validator validator() {
        return Validation
                .byProvider(HibernateValidator.class)
                .configure() // 开始配置Validator
                .failFast(true) // 快速失败，即检查到错误就直接视为失败
                .buildValidatorFactory()
                .getValidator();
    }

}
```

Spring Validation是通过不同的注解，实现不同的检查功能，常用的检查注解有：

- `@NotNull`：不允许为`null`
- `@NotEmpty`：不允许为空（长度为0的字符串）
  - 仅适用于**字符串**类型的请求参数
  - 可以与`@NotNull`同时使用，且当通过`@NotNull`后再执行检查
- `@NotBlank`：不允许为空白
  - 仅适用于**字符串**类型的请求参数
  - 可以与`@NotNull`同时使用，且当通过`@NotNull`后再执行检查
- `@Pattern`：使用正则表达式检查，需要通过此注解的`regexp`属性来配置正则表达式
  - 仅适用于**字符串**类型的请求参数
  - 可以与`@NotNull`同时使用，且当通过`@NotNull`后再执行检查
- `@Min`：值不得小于多少
  - 仅适用于**数值**类型的请求参数
  - 可以与`@NotNull`同时使用，且当通过`@NotNull`后再执行检查
- `@Max`：值不得大于多少
  - 仅适用于**数值**类型的请求参数
  - 可以与`@NotNull`同时使用，且当通过`@NotNull`后再执行检查
- `@Range`：值必须在指定的区间范围内
  - 仅适用于**数值**类型的请求参数
  - 可以与`@NotNull`同时使用，且当通过`@NotNull`后再执行检查

如果处理请求的方法的参数不是封装的数据类型，需要进行检查时，需要先在当前类上添加`@Validated`注解，然后，再在参数上添加相关的检查注解，例如：

```java
@Slf4j
@RestController
@RequestMapping("/brands")
@Api(tags = "02. 品牌管理模块")
@Validated // 【新增】
public class BrandController {

    @Autowired
    private IBrandService brandService;

    public BrandController() {
        log.info("创建控制器：BrandController");
    }

    // http://localhost:9080/brands/test/delete
    @Deprecated
    @ApiOperation("测试：删除品牌")
    @ApiOperationSupport(order = 901)
    @ApiImplicitParam(name = "enable", dataType = "int", paramType = "query") // 【新增】
    @GetMapping("/test/delete")
    // 【新增】以下方法的参数上添加了检查注解
    public String delete(@Range(max = 1) Integer enable) {
        log.debug("接收到【删除品牌（测试）】的请求");
        log.debug("enable = {}", enable);
        throw new RuntimeException("此接口仅用于测试，并未实现任何功能！");
    }

}
```

使用这种方式检查请求参数时，如果检查不通过，将抛出`ConstraintViolationException`异常，所以，还需要在**全局异常处理器（`GlobalExceptionHandler`）**中添加：

```java
@ExceptionHandler
public JsonResult<Void> handleConstraintViolationException(ConstraintViolationException e) {
    log.debug("处理ConstraintViolationException");

    Integer serviceCode = ServiceCode.ERR_BAD_REQUEST.getValue();

    StringBuilder messageBuilder = new StringBuilder();
    Set<ConstraintViolation<?>> constraintViolations = e.getConstraintViolations();
    for (ConstraintViolation<?> constraintViolation : constraintViolations) {
        messageBuilder.append(constraintViolation.getMessage());
    }

    String message = messageBuilder.toString();
    return JsonResult.fail(serviceCode, message);
}
```







