# 1. 显示品牌列表

由于`BrandMapper`已经实现了“查询品牌列表”功能，所以，Mapper层无需处理。

在`IBrandService`接口中添加：

```java
/**
 * 查询品牌列表
 *
 * @return 品牌列表，如果没有匹配的品牌，将返回长度为0的列表
 */
List<BrandListItemVO> list();
```

在`BrandServiceImpl`中调用Mapper对象的查询方法直接实现：

```java
@Override
public List<BrandListItemVO> list() {
    log.debug("开始处理【查询品牌列表】的业务");
    return brandMapper.list();
}
```

在`BrandServiceTests`中测试：

```java
@Test
void testList() {
    List<?> list = service.list();
    log.debug("查询品牌列表，查询结果中的数据的数量：{}", list.size());
    for (Object brand : list) {
        log.debug("{}", brand);
    }
}
```

然后，在`BrandController`中添加处理请求的方法：

```java
// http://localhost:9080/brands
@ApiOperation("查询品牌列表")
@ApiOperationSupport(order = 400)
@GetMapping("")
public JsonResult<List<BrandListItemVO>> list() {
    log.debug("开始处理【查询品牌列表】的请求……");
    List<BrandListItemVO> list = brandService.list();
    return JsonResult.ok(list);
}
```

完成后，通过Knife4j的API文档可以进行调试（测试访问）。

# 2. SSO（Single Sign On：单点登录）

在集群甚至分布式系统中，通常只有某1种服务提供登录认证，无论是其它哪个服务需要用户登录，用户都应该在此专门提供登录认证的服务器端进行认证，并且，认证结果对于其它所有服务都是有效的！

SSO的典型实现方案就是使用Token。

关于商品管理的相关功能，也应该是需要经过认证的（需要先登录），并且，可能也会采取某些权限控制！

需要将`passport`项目的相关代码复制到`product`项目中：

- `pom.xml`中的依赖：`spring-boot-starter-security`、`fastjson`、`jjwt`
- `ServiceCode`：补充新的枚举值
- `LoginPrincipal`
- `application.properties`中的配置：关于JWT的配置，且`secretKey`必须相同
- `JwtAuthorizationFilter`过滤器
- `SecurityConfiguration`配置类
  - 删除`PasswordEncoder`的`@Bean`方法
  - 删除`AuthenticationManager`的`@Bean`方法
    - 必须删除，否则，在`product`项目中执行测试时，会出现内存溢出
  - 调整配置的白名单
- `GlobalExceptionHandler`：至少补充处理`AccessDeniedException`

完成后，可以先通过`product`项目的在线API文档进行测试访问，在没有携带JWT的情况下，所有请求都会响应`403`错误，需要先在`passport`项目的在线API文档中执行登录，得到JWT数据，并配置到`product`项目的API文档中，再次访问，则可以正常访问！

# 3. 显示类别列表

关于“类别”的显示，需要注意：通常并不需要一次性将所有的、各层级的类别全部查询或显示出来，只需要查询特定的一些类别，例如：查询所有1级类别，或查询某个1级类别的子级类别列表等。

目前，在Mapper层已经实现了`List<CategoryListItemVO> listByParentId(Long parentId);`，所以，Mapper层无需再开发。

则，在`ICategoryService`中添加：

```java
/**
 * 根据父级类别的id查询类别列表
 *
 * @param parentId 父级类别的id
 * @return 类别列表
 */
List<CategoryListItemVO> listByParentId(Long parentId);
```

在`CategoryServiceImpl`中实现：

```java
@Override
public List<CategoryListItemVO> listByParentId(Long parentId) {
    log.debug("开始处理【根据父级类别查询子级类别列表】的业务");
    return categoryMapper.listByParentId(parentId);
}
```

在`CategoryServiceTests`中测试：

```java
@Test
public void testListByParentId() {
    Long parentId = 0L;
    List<?> list = service.listByParentId(parentId);
    log.info("查询列表完成，结果集中的数据的数量={}", list.size());
    for (Object item : list) {
        log.info("{}", item);
    }
}
```

在`CategoryController`中添加处理请求的方法：

```java
// http://localhost:9080/categories/list-by-parent
@ApiOperation("根据父级类别查询子级类别列表")
@ApiOperationSupport(order = 410)
@ApiImplicitParam(name = "parentId", value = "父级类别id，如果是一级类别，则此参数值应该为0",
            required = true, dataType = "long")
@GetMapping("/list-by-parent")
public JsonResult<List<CategoryListItemVO>> listByParentId(Long parentId) {
    if (parentId == null || parentId < 0) {
        parentId = 0L;
    }
    List<CategoryListItemVO> list = categoryService.listByParentId(parentId);
    return JsonResult.ok(list);
}
```

完成后，重启项目，通过API文档应该可以测试访问。

# 4. 添加属性模板

由于服务器端已经实现了“添加属性模板”的功能，所以，只需完成前端界面即可。

# 5. 显示属性模板列表

此前已经实现了Mapper层的查询，接下来，需要实现Service层、Controller层和前端界面的处理！

# 6. 在添加属性界面中显示属性模板列表的下拉菜单



# 作业

- 完成“删除品牌”功能（从界面上点击按钮完成）
- 完成“启用品牌”功能（从界面上点击按钮完成）
- 完成“禁用品牌”功能（从界面上点击按钮完成）
- 完成“启用类别”功能（从界面上点击按钮完成）
- 完成“禁用类别”功能（从界面上点击按钮完成）
- 完成“显示类别（是否显示在导航栏）”功能（从界面上点击按钮完成）
- 完成“隐藏类别（是否显示在导航栏）”功能（从界面上点击按钮完成）
- 完成“删除类别”功能（从界面上点击按钮完成）
- 完成“删除相册”功能（从界面上点击按钮完成）
- 完成“删除属性模板”功能（从界面上点击按钮完成）



