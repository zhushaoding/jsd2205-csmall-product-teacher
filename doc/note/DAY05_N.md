# 1. 消息摘要算法与密码加密

在Spring Boot项目中，提供了`DigestUtils`工具类，此工具类的方法可以轻松实现“使用MD5算法”进行运算，从而，可以实现将原始密码进行加密，得到一个加密后的结果。

```java
package cn.tedu.csmall.product;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.util.DigestUtils;

@Slf4j
public class DigestTests {

    @Test
    public void testEncode() {
        String rawPassword = "123456";
        String encodedPassword = DigestUtils
                .md5DigestAsHex(rawPassword.getBytes());
        log.debug("原始密码={}，MD5运算结果={}", rawPassword, encodedPassword);
    }

}
```

首先，对密码进行加密后，再存储，是非常有必要的！并且，主要的防范对象通常是内部员工！

需要注意，对密码进行加密处理时，**不可以使用加密算法**！因为，所有的加密算法都是可以加密，也可以解密的！加密算法的核心价值在于**保证数据在传输过程中是安全的**，并不保证数据存储的安全！

对需要存储的密码进行加密处理时，应该使用**消息摘要算法**，其本质是一种哈希算法，是**不可逆向运算**的！













