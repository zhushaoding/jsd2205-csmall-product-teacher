# Mybatis的缓存机制

Mybatis框架内置了一级缓存机制与二级缓存机制。

Mybatis框架的一级缓存又称之为**会话（Session）缓存**，默认是开启的，且无法关闭！

一级缓存必须满足：同一个SqlSession、同一个Mapper对象、执行相同的查询、且参数相同！

通过测试并观察日志【参见下文】，可以看到，第1次执行查询，但是，第2次并没有真正的执行查询，并且，2次查询结果的hashCode值是完全相同的！也就是说：Mybatis只执行了第1次的查询，当执行第2次的代码时，并没有真正连接到MySQL数据库执行查询，而是将第1次的查询结果直接返回了！也可以说：Mybatis在第1次查询后，就把查询结果缓存下来了！

一级缓存会因为以下任一原因消失：

- 调用`SqlSession`对象的`clearCache()`方法，将清空缓存
- 当前执行了任何写操作（增/删/改），无论任何数据有没有发生变化，都会清空缓存

```java
package cn.tedu.csmall.product;

import cn.tedu.csmall.product.mapper.BrandMapper;
import cn.tedu.csmall.product.pojo.entity.Brand;
import cn.tedu.csmall.product.pojo.vo.BrandStandardVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@Slf4j
@SpringBootTest
public class MybatisCacheTests {

    @Autowired
    SqlSessionFactory sqlSessionFactory;

    @Test
    void testL1Cache() {
        SqlSession sqlSession = sqlSessionFactory.openSession();
        BrandMapper brandMapper = sqlSession.getMapper(BrandMapper.class);

        Long id = 1L;
        log.debug("开始执行第1次【id={}】的查询……", id);
        BrandStandardVO result1 = brandMapper.getStandardById(id);
        log.debug("第1次的查询结果：{}", result1);
        log.debug("第1次的查询结果的hashCode：{}", result1.hashCode());

        // sqlSession.clearCache();
        // log.debug("已经调用clearCache()方法清除缓存！");

        log.debug("开始执行第2次【id={}】的查询……", id);
        BrandStandardVO result2 = brandMapper.getStandardById(id);
        log.debug("第2次的查询结果：{}", result2);
        log.debug("第2次的查询结果的hashCode：{}", result2.hashCode());

        id = 4L;
        log.debug("开始执行第1次【id={}】的查询……", id);
        BrandStandardVO result3 = brandMapper.getStandardById(id);
        log.debug("第2次的查询结果：{}", result3);
        log.debug("第2次的查询结果的hashCode：{}", result3.hashCode());

        // sqlSession.clearCache();
        // log.debug("已经调用clearCache()方法清除缓存！");

        log.debug("准备修改【id={}】的数据……", 40000);
        Brand brand = new Brand();
        brand.setId(40000L);
        brand.setSort(99);
        brandMapper.updateById(brand);
        log.debug("完成修改【id={}】的数据", 40000);

        log.debug("开始执行第2次【id={}】的查询……", id);
        BrandStandardVO result4 = brandMapper.getStandardById(id);
        log.debug("第2次的查询结果：{}", result4);
        log.debug("第2次的查询结果的hashCode：{}", result4.hashCode());

        id = 1L;
        log.debug("开始执行第3次【id={}】的查询……", id);
        BrandStandardVO result5 = brandMapper.getStandardById(id);
        log.debug("第3次的查询结果：{}", result5);
        log.debug("第3次的查询结果的hashCode：{}", result5.hashCode());
    }

}
```

Mybatis框架的二级缓存也称之为**namespace缓存**，是作用于某个namespace的！

二级缓存的应用优先级高于一级缓存，也就是说，Mybatis框架将优先从二级缓存中查找数据，如果命中，将返回，如果未命中，则从一级缓存中查找数据，如果命中，将返回，如果仍未命中，将执行真正的查询！

在使用Spring Boot与Mybatis集成的框架的项目中，二级缓存默认是全局开启的，各namespace默认未开启，如果需要开启，需要在XML文件中添加`<cache/>`标签，则表示当前XML中所有的查询都开启了二级缓存！另外，在每个查询上都有`useCache`属性，默认为`true`，如果在同一个namespace中，有些查询需要使用二级缓存，也有一些并不需要使用，则可以将不需要使用二级缓存的查询配置为`useCache="false"`。

当启用了二缓存后，每次尝试查询时，日志中都会出现 `Cache Hit Ratio` 的记录！

```java
@Autowired
BrandMapper brandMapper;

@Test
void testL2Cache() {
    Long id = 1L;
    log.debug("开始执行第1次【id={}】的查询……", id);
    BrandStandardVO result1 = brandMapper.getStandardById(id);
    log.debug("第1次的查询结果：{}", result1);
    log.debug("第1次的查询结果的hashCode：{}", result1.hashCode());

    log.debug("准备修改【id={}】的数据……", 40000);
    Brand brand = new Brand();
    brand.setId(40000L);
    brand.setSort(99);
    brandMapper.updateById(brand);
    log.debug("完成修改【id={}】的数据", 40000);

    log.debug("开始执行第2次【id={}】的查询……", id);
    BrandStandardVO result2 = brandMapper.getStandardById(id);
    log.debug("第2次的查询结果：{}", result2);
    log.debug("第2次的查询结果的hashCode：{}", result2.hashCode());

    id = 4L;
    log.debug("开始执行第1次【id={}】的查询……", id);
    BrandStandardVO result3 = brandMapper.getStandardById(id);
    log.debug("第2次的查询结果：{}", result3);
    log.debug("第2次的查询结果的hashCode：{}", result3.hashCode());

    log.debug("开始执行第2次【id={}】的查询……", id);
    BrandStandardVO result4 = brandMapper.getStandardById(id);
    log.debug("第2次的查询结果：{}", result4);
    log.debug("第2次的查询结果的hashCode：{}", result4.hashCode());

    id = 1L;
    log.debug("开始执行第3次【id={}】的查询……", id);
    BrandStandardVO result5 = brandMapper.getStandardById(id);
    log.debug("第3次的查询结果：{}", result5);
    log.debug("第3次的查询结果的hashCode：{}", result5.hashCode());
}
```

二级缓存的数据是存储在磁盘上的，需要查询结果的数据类型实现了`Serilizable`接口，如果未实现此接口，则查询时直接报错：

```
org.apache.ibatis.cache.CacheException: Error serializing object.  Cause: java.io.NotSerializableException: cn.tedu.csmall.product.pojo.vo.BrandStandardVO
```

与一级缓存相同，只要发生了任何写操作（增/删/改），都会自动清除缓存数据！