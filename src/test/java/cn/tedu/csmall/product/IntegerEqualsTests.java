package cn.tedu.csmall.product;

import org.junit.jupiter.api.Test;

public class IntegerEqualsTests {

    @Test
    public void test() {
        System.out.println("对Integer类型的值进行对比时");
        System.out.println("如果值在 [-128, 127] 区间内，可以使用 == 或 equals()");
        System.out.println("如果值不在 [-128, 127] 区间内，只能使用 equals()");
        System.out.println();

        Integer i1 = 127;
        Integer i2 = 127;
        System.out.println(i1 == i2);
        System.out.println(i1.equals(i2));

        System.out.println();

        Integer i3 = 128;
        Integer i4 = 128;
        System.out.println(i3 == i4);
        System.out.println(i3.equals(i4));

        System.out.println();

        Integer i5 = -128;
        Integer i6 = -128;
        System.out.println(i5 == i6);
        System.out.println(i5.equals(i6));

        System.out.println();

        Integer i7 = -129;
        Integer i8 = -129;
        System.out.println(i7 == i8);
        System.out.println(i7.equals(i8));
    }

}
