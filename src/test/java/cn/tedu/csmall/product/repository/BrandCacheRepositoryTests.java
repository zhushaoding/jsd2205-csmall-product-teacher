package cn.tedu.csmall.product.repository;

import cn.tedu.csmall.product.pojo.vo.BrandStandardVO;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@Slf4j
@SpringBootTest
public class BrandCacheRepositoryTests {

    @Autowired
    IBrandCacheRepository repository;

    @Test
    void testSave() {
        BrandStandardVO brandStandardVO = new BrandStandardVO();
        brandStandardVO.setId(1L);
        brandStandardVO.setName("华为");

        repository.save(brandStandardVO);
        log.debug("存入数据完成！");
    }

    @Test
    void testGet() {
        Long id = 1L;
        BrandStandardVO brandStandardVO = repository.get(id);
        log.debug("获取数据完成：{}", brandStandardVO);
    }

}
