package cn.tedu.csmall.product;

import cn.tedu.csmall.product.mapper.BrandMapper;
import cn.tedu.csmall.product.pojo.entity.Brand;
import cn.tedu.csmall.product.pojo.vo.BrandStandardVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@Slf4j
@SpringBootTest
public class MybatisCacheTests {

    @Autowired
    SqlSessionFactory sqlSessionFactory;

    @Test
    void testL1Cache() {
        SqlSession sqlSession = sqlSessionFactory.openSession();
        BrandMapper brandMapper = sqlSession.getMapper(BrandMapper.class);

        Long id = 1L;
        log.debug("开始执行第1次【id={}】的查询……", id);
        BrandStandardVO result1 = brandMapper.getStandardById(id);
        log.debug("第1次的查询结果：{}", result1);
        log.debug("第1次的查询结果的hashCode：{}", result1.hashCode());

        // sqlSession.clearCache();
        // log.debug("已经调用clearCache()方法清除缓存！");

        log.debug("开始执行第2次【id={}】的查询……", id);
        BrandStandardVO result2 = brandMapper.getStandardById(id);
        log.debug("第2次的查询结果：{}", result2);
        log.debug("第2次的查询结果的hashCode：{}", result2.hashCode());

        id = 4L;
        log.debug("开始执行第1次【id={}】的查询……", id);
        BrandStandardVO result3 = brandMapper.getStandardById(id);
        log.debug("第2次的查询结果：{}", result3);
        log.debug("第2次的查询结果的hashCode：{}", result3.hashCode());

        // sqlSession.clearCache();
        // log.debug("已经调用clearCache()方法清除缓存！");

        // log.debug("准备修改【id={}】的数据……", 40000);
        // Brand brand = new Brand();
        // brand.setId(40000L);
        // brand.setSort(99);
        // brandMapper.updateById(brand);
        // log.debug("完成修改【id={}】的数据", 40000);

        log.debug("开始执行第2次【id={}】的查询……", id);
        BrandStandardVO result4 = brandMapper.getStandardById(id);
        log.debug("第2次的查询结果：{}", result4);
        log.debug("第2次的查询结果的hashCode：{}", result4.hashCode());

        id = 1L;
        log.debug("开始执行第3次【id={}】的查询……", id);
        BrandStandardVO result5 = brandMapper.getStandardById(id);
        log.debug("第3次的查询结果：{}", result5);
        log.debug("第3次的查询结果的hashCode：{}", result5.hashCode());
    }

    @Autowired
    BrandMapper brandMapper;

    @Test
    void testL2Cache() {
        Long id = 1L;
        log.debug("开始执行第1次【id={}】的查询……", id);
        BrandStandardVO result1 = brandMapper.getStandardById(id);
        log.debug("第1次的查询结果：{}", result1);
        log.debug("第1次的查询结果的hashCode：{}", result1.hashCode());

        log.debug("准备修改【id={}】的数据……", 40000);
        Brand brand = new Brand();
        brand.setId(40000L);
        brand.setSort(99);
        brandMapper.updateById(brand);
        log.debug("完成修改【id={}】的数据", 40000);

        log.debug("开始执行第2次【id={}】的查询……", id);
        BrandStandardVO result2 = brandMapper.getStandardById(id);
        log.debug("第2次的查询结果：{}", result2);
        log.debug("第2次的查询结果的hashCode：{}", result2.hashCode());

        id = 4L;
        log.debug("开始执行第1次【id={}】的查询……", id);
        BrandStandardVO result3 = brandMapper.getStandardById(id);
        log.debug("第2次的查询结果：{}", result3);
        log.debug("第2次的查询结果的hashCode：{}", result3.hashCode());

        log.debug("开始执行第2次【id={}】的查询……", id);
        BrandStandardVO result4 = brandMapper.getStandardById(id);
        log.debug("第2次的查询结果：{}", result4);
        log.debug("第2次的查询结果的hashCode：{}", result4.hashCode());

        id = 1L;
        log.debug("开始执行第3次【id={}】的查询……", id);
        BrandStandardVO result5 = brandMapper.getStandardById(id);
        log.debug("第3次的查询结果：{}", result5);
        log.debug("第3次的查询结果的hashCode：{}", result5.hashCode());
    }

}
