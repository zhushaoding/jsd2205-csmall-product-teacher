package cn.tedu.csmall.product;

import cn.tedu.csmall.product.pojo.entity.Brand;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.ListOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Slf4j
@SpringBootTest
public class RedisTests {

    @Autowired
    RedisTemplate<String, Serializable> redisTemplate;

    @Test
    void testOpsForValueSet() {
        ValueOperations<String, Serializable> opsForValue = redisTemplate.opsForValue();
        opsForValue.set("phone", "13900138000");
    }

    @Test
    void testOpsForValueGet() {
        ValueOperations<String, Serializable> opsForValue = redisTemplate.opsForValue();
        String key = "phone";
        Serializable value = opsForValue.get(key);
        log.debug("读取数据：key={}, value={}", key, value);
    }

    @Test
    void testOpsForValueSetObject() {
        Brand brand = new Brand();
        brand.setName("华为");
        brand.setPinyin("huawei");
        brand.setSort(998);

        ValueOperations<String, Serializable> opsForValue = redisTemplate.opsForValue();
        String key = "brand-huawei";
        opsForValue.set(key, brand);
    }

    @Test
    void testOpsForValueGetObject() {
        ValueOperations<String, Serializable> opsForValue = redisTemplate.opsForValue();
        String key = "brand-huawei";
        Serializable value = opsForValue.get(key);
        log.debug("读取数据：key={}, value={}", key, value);

        if (value != null) {
            log.debug("读取到的数据的类型：{}", value.getClass().getName());
        }
    }

    @Test
    void testKeys() {
        Set<String> keys = redisTemplate.keys("brand*");
        for (String key : keys) {
            log.debug("key = {}", key);
        }
    }

    @Test
    void testDelete() {
        String key = "age";
        Boolean result = redisTemplate.delete(key);
        log.debug("根据 key={} 执行删除完成，结果：{}", key, result);
    }

    @Test
    void testDeleteAll() {
        Set<String> keys = redisTemplate.keys("*");
        Long deleteCount = redisTemplate.delete(keys);
        log.debug("批量删除完成，删除的数量：{}", deleteCount);
    }

    @Test
    void testRightPushList() {
        // push：压栈（存入数据）
        // pop：弹栈（拿走数据）
        // 使用RedisTemplate向Redis存入List数据：
        // 1. 需要调用 opsForList() 得到 ListOperations 对象
        // 2. ListOperations每次只能存入1个列表项数据
        List<Brand> brands = new ArrayList<>();
        for (int i = 1; i <= 8; i++) {
            Brand brand = new Brand();
            brand.setId(i + 0L);
            brand.setName("测试品牌" + i);
            brands.add(brand);
        }

        ListOperations<String, Serializable> opsForList = redisTemplate.opsForList();
        String key = "brandList";
        for (Brand brand : brands) {
            opsForList.rightPush(key, brand);
        }
    }

    @Test
    void testListSize() {
        ListOperations<String, Serializable> opsForList = redisTemplate.opsForList();
        String key = "brandList";
        Long size = opsForList.size(key);
        log.debug("列表 key={} 的长度（元素数量）为：{}", key, size);
    }

    @Test
    void testListRange() {
        ListOperations<String, Serializable> opsForList = redisTemplate.opsForList();
        String key = "brandList";
        long start = 0L;
        long end = -1L;
        List<Serializable> list = opsForList.range(key, start, end);
        for (Serializable serializable : list) {
            log.debug("列表项：{}", serializable);
        }
    }

}




