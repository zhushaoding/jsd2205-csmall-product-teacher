package cn.tedu.csmall.product.service;

import cn.tedu.csmall.product.ex.ServiceException;
import cn.tedu.csmall.product.pojo.dto.AttributeTemplateAddNewDTO;
import cn.tedu.csmall.product.pojo.dto.AttributeTemplateUpdateInfoDTO;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@Slf4j
@SpringBootTest
public class AttributeTemplateServiceTests {

    @Autowired
    IAttributeTemplateService service;

    @Test
    void testAddNew() {
        AttributeTemplateAddNewDTO attributeTemplateAddNewDTO
                = new AttributeTemplateAddNewDTO();
        attributeTemplateAddNewDTO.setName("大米手机的属性模版");

        try {
            service.addNew(attributeTemplateAddNewDTO);
            log.debug("添加属性模板成功！");
        } catch (ServiceException e) {
            log.debug(e.getMessage());
        }
    }

    @Test
    void testDeleteById() {
        try {
            Long id = 1L;
            service.deleteById(id);
            log.debug("根据id={}删除属性模板成功！");
        } catch (ServiceException e) {
            log.debug(e.getMessage());
        }
    }

    @Test
    void testUpdateInfoById() {
        try {
            Long id = 1L;
            AttributeTemplateUpdateInfoDTO attributeTemplateUpdateInfoDTO
                    = new AttributeTemplateUpdateInfoDTO();
            attributeTemplateUpdateInfoDTO.setKeywords("测试关键字");
            attributeTemplateUpdateInfoDTO.setSort(66);
            attributeTemplateUpdateInfoDTO.setPinyin("ce shi guan jian zi");
            service.updateInfoById(id, attributeTemplateUpdateInfoDTO);
            log.debug("修改属性模板基本资料成功");
        } catch (ServiceException e) {
            log.debug(e.getMessage());
        }
    }

    @Test
    public void testList() {
        List<?> list = service.list();
        log.debug("查询列表完成，结果集中的数据的数量={}", list.size());
        for (Object item : list) {
            log.debug("{}", item);
        }
    }

}
