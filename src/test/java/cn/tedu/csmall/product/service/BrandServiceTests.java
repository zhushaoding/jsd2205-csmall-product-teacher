package cn.tedu.csmall.product.service;

import cn.tedu.csmall.product.ex.ServiceException;
import cn.tedu.csmall.product.pojo.dto.BrandAddNewDTO;
import cn.tedu.csmall.product.pojo.vo.BrandStandardVO;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.sql.rowset.serial.SerialException;
import java.util.List;

@Slf4j
@SpringBootTest
public class BrandServiceTests {

    @Autowired
    IBrandService service;

    @Test
    void testAddNew() {
        BrandAddNewDTO brandAddNewDTO = new BrandAddNewDTO();
        brandAddNewDTO.setName("海尔");

        try {
            service.addNew(brandAddNewDTO);
            log.debug("添加品牌成功！");
        } catch (ServiceException e) {
            log.debug("serviceCode : {}", e.getServiceCode());
            log.debug("message : {}", e.getMessage());
        }
    }
    
    @Test
    void testDeleteById() {
        Long id = 1L;
        
        try {
            service.deleteById(id);
            log.debug("删除成功！");
        } catch (ServiceException e) {
            log.debug("serviceCode: {}", e.getServiceCode());
            log.debug("message: {}", e.getMessage());
        }
    }

    @Test
    void testSetEnable() {
        Long id = 2L;

        try {
            service.setEnable(id);
            log.debug("启用成功！");
        } catch (ServiceException e) {
            log.debug("serviceCode: {}", e.getServiceCode());
            log.debug("message: {}", e.getMessage());
        }
    }

    @Test
    void testSetDisable() {
        Long id = 2L;

        try {
            service.setDisable(id);
            log.debug("禁用成功！");
        } catch (ServiceException e) {
            log.debug("serviceCode: {}", e.getServiceCode());
            log.debug("message: {}", e.getMessage());
        }
    }

    @Test
    void testGetStandardById() {
        Long id = 30000L;
        try {
            BrandStandardVO result = service.getStandardById(id);
            log.debug("根据id={}查询品牌详情，结果={}", id, result);
        } catch (ServiceException e) {
            log.debug("serviceCode: {}", e.getServiceCode());
            log.debug("message: {}", e.getMessage());
        }
    }

    @Test
    void testList() {
        List<?> list = service.list();
        log.debug("查询品牌列表，查询结果中的数据的数量：{}", list.size());
        for (Object brand : list) {
            log.debug("{}", brand);
        }
    }

    @Test
    void testLoadBrandsToCache() {
        service.loadBrandsToCache();
        log.debug("将品牌数据加载到缓存中，完成！");
    }

}
