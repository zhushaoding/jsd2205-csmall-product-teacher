package cn.tedu.csmall.product.service;

import cn.tedu.csmall.product.ex.ServiceException;
import cn.tedu.csmall.product.pojo.dto.CategoryAddNewDTO;
import cn.tedu.csmall.product.pojo.vo.CategoryStandardVO;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@Slf4j
@SpringBootTest
public class CategoryServiceTests {

    @Autowired
    ICategoryService service;

    @Test
    void testAddNew() {
        CategoryAddNewDTO categoryAddNewDTO = new CategoryAddNewDTO();
        categoryAddNewDTO.setName("热带水果");
        categoryAddNewDTO.setParentId(25L);

        try {
            service.addNew(categoryAddNewDTO);
            log.debug("添加成功！");
        } catch (ServiceException e) {
            log.debug("serviceCode : " + e.getServiceCode());
            log.debug("message : " + e.getMessage());
        }
    }

    @Test
    void testDeleteById() {
        Long id = 26L;

        try {
            service.deleteById(id);
            log.debug("删除成功！");
        } catch (ServiceException e) {
            log.debug("serviceCode : " + e.getServiceCode());
            log.debug("message : " + e.getMessage());
        }
    }

    @Test
    public void testGetStandardById() {
        Long id = 2L;
        try {
            CategoryStandardVO category = service.getStandardById(id);
            log.debug("根据id={}查询完成，查询结果={}", id, category);
        } catch (ServiceException e) {
            log.debug("serviceCode : " + e.getServiceCode());
            log.debug("message : " + e.getMessage());
        }
    }

    @Test
    public void testListByParentId() {
        Long parentId = 0L;
        List<?> list = service.listByParentId(parentId);
        log.info("查询列表完成，结果集中的数据的数量={}", list.size());
        for (Object item : list) {
            log.info("{}", item);
        }
    }

}
