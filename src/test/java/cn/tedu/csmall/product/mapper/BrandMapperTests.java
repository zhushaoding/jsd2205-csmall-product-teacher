package cn.tedu.csmall.product.mapper;

import cn.tedu.csmall.product.pojo.entity.Brand;
import cn.tedu.csmall.product.pojo.vo.BrandStandardVO;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@SpringBootTest
public class BrandMapperTests {

    @Autowired
    BrandMapper mapper;

    @Test
    void testInsert() {
        Brand brand = new Brand();
        brand.setName("测试品牌123");

        log.debug("插入品牌之前，参数对象={}", brand);
        int rows = mapper.insert(brand);
        log.debug("插入品牌完成，受影响的行数={}", rows);
        log.debug("插入品牌之后，参数对象={}", brand);
    }

    @Test
    void testDeleteById() {
        Long id = 4L;
        int rows = mapper.deleteById(id);
        log.debug("根据id删除品牌完成，受影响的行数：{}", rows);
    }

    @Test
    void testDeleteByIds() {
        List<Long> ids = new ArrayList<>();
        ids.add(8L);
        ids.add(9L);
        ids.add(10L);
        ids.add(100L);
        ids.add(1000L);
        ids.add(10000L);

        int rows = mapper.deleteByIds(ids);
        log.debug("根据id批量删除品牌完成，受影响的行数={}", rows);
    }

    @Test
    void testUpdateById() {
        Brand brand = new Brand();
        brand.setId(11L);
        brand.setName("测试品牌-011");
        brand.setPinyin("ceshipinpai-011");
        //brand.setLogo("http://www.alibaba.com/brand-11.png");

        int rows = mapper.updateById(brand);
        log.debug("根据id修改品牌完成，受影响的行数={}", rows);
    }

    @Test
    void testCount() {
        int count = mapper.count();
        log.debug("统计品牌的数量完成，品牌的数量={}", count);
    }

    @Test
    void testCountByName() {
        String name = "旺仔";
        int count = mapper.countByName(name);
        log.debug("根据品牌名称【{}】统计品牌数据的数量，结果={}", name, count);
    }

    @Test
    void testGetStandardById() {
        Long id = 3L;
        BrandStandardVO result = mapper.getStandardById(id);
        log.debug("根据id={}查询品牌详情，结果={}", id, result);
    }

    @Test
    void testList() {
        List<?> list = mapper.list();
        log.debug("查询品牌列表，查询结果中的数据的数量：{}", list.size());
        for (Object brand : list) {
            log.debug("{}", brand);
        }
    }

}
