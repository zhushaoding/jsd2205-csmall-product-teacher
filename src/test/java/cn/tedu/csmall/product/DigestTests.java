package cn.tedu.csmall.product;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.util.DigestUtils;

@Slf4j
public class DigestTests {

    @Test
    public void testEncode() {
        String rawPassword = "123456";
        String salt = "jhfdiu78543hjfdo8";
        String encodedPassword = DigestUtils
                .md5DigestAsHex((salt + rawPassword + salt).getBytes());
        log.debug("原始密码={}，MD5运算结果={}", rawPassword, encodedPassword);

        // 123456
        // 原来：e10adc3949ba59abbe56e057f20f883e

        // 123456jhfdiu78543hjfdo8
        // 加盐：4eddcdd18bc7d8af88916114bf763189

        // jhfdiu78543hjfdo8123456jhfdiu78543hjfdo8

        // 1234567890ABCDEFGHIJKLMN
        // 41217c45889b5378c3dad3879d7bfac9
    }

}
