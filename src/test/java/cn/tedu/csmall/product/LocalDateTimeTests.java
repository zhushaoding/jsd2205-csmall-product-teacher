package cn.tedu.csmall.product;

import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Random;

public class LocalDateTimeTests {

    @Test
    public void test() {
        LocalDateTime now = LocalDateTime.now();
        System.out.println(now);

        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmssSSS");
        String dateTimeString = dateTimeFormatter.format(now);
        System.out.println(dateTimeString);

        Random random = new Random();
        int i = random.nextInt(89) + 10;
        System.out.println("i = " + i);

        Long dateTimeValue = Long.valueOf(dateTimeString + i);
        System.out.println(dateTimeValue);
    }

}
