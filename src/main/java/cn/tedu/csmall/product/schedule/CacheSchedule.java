package cn.tedu.csmall.product.schedule;

import cn.tedu.csmall.product.service.IBrandService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * 处理缓存的计划任务类
 *
 * @author java@tedu.cn
 * @version 0.0.1
 */
@Slf4j
@Component
public class CacheSchedule {

    @Autowired
    private IBrandService brandService;

    // 关于@Scheduled注解的配置
    // 以下的各属性不可以同时配置
    // >> fixedRate：每间隔多少毫秒执行1次
    // >> fixedDelay：每执行结束后，过多少毫秒执行1次
    // >> cron：使用1个字符串，字符串中包含6~7个值，各值之间使用空格分隔
    // >> >> 各值分别表示：秒 分 时 日 月 周（星期） [年]
    // >> >> 例如：cron = "56 34 12 20 1 ? 2023"，表示"2023年1月20日12:34:56秒将执行，无论这一天是星期几"
    // >> >> 以上各个值，均可使用星号（*）作为通配符，表示任意值
    // >> >> 在“日”和“周”位置，还可以使用问号（?），表示不关心具体值
    // >> >> 以上各个值，还可以使用“x/x”格式的值，例如在分钟位置使用 1/5，表示分钟值为1时执行，且每间隔5个单位（分钟）执行1次
    @Scheduled(fixedRate = 1 * 60 * 60 * 1000)
    public void loadBrandsToCache() {
        log.debug("开始执行计划任务……");
        brandService.loadBrandsToCache();
        log.debug("本次计划任务执行完成！");
    }

}
