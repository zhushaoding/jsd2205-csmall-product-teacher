package cn.tedu.csmall.product.controller;

import cn.tedu.csmall.product.pojo.dto.AlbumAddNewDTO;
import cn.tedu.csmall.product.pojo.dto.AlbumUpdateDTO;
import cn.tedu.csmall.product.pojo.vo.AlbumListItemVO;
import cn.tedu.csmall.product.pojo.vo.AlbumStandardVO;
import cn.tedu.csmall.product.service.IAlbumService;
import cn.tedu.csmall.product.web.JsonResult;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 处理相册相关请求的控制器
 *
 * @author java@tedu.cn
 * @version 0.0.1
 */
@Slf4j
@RestController
@RequestMapping("/albums")
@Api(tags = "04. 相册管理模块")
public class AlbumController {

    @Autowired
    private IAlbumService albumService;

    public AlbumController() {
        log.info("创建控制器：AlbumController");
    }

    // 添加相册
    // http://localhost:9080/albums/add-new
    @ApiOperation("添加相册")
    @ApiOperationSupport(order = 100)
    @PostMapping("/add-new")
    public JsonResult<Void> addNew(@Validated AlbumAddNewDTO albumAddNewDTO) {
        log.debug("开始处理【添加相册】的请求：{}", albumAddNewDTO);
        albumService.addNew(albumAddNewDTO);
        return JsonResult.ok();
    }

    // http://localhost:9080/albums/9527/delete
    @ApiOperation("删除相册")
    @ApiOperationSupport(order = 200)
    @ApiImplicitParam(name = "id", value = "相册id", required = true, dataType = "long")
    @PostMapping("/{id:[0-9]+}/delete")
    public JsonResult<Void> delete(@PathVariable Long id) {
        log.debug("开始处理【删除相册】的请求：id={}", id);
        albumService.deleteById(id);
        return JsonResult.ok();
    }

    // http://localhost:9080/albums/3/update
    @ApiOperation("修改相册详情")
    @ApiOperationSupport(order = 300)
    @ApiImplicitParam(name = "id", value = "相册id", required = true, dataType = "long")
    @PostMapping("/{id:[0-9]+}/update")
    public JsonResult<Void> updateById(@PathVariable Long id, AlbumUpdateDTO albumUpdateDTO) {
        log.debug("开始处理【修改相册详情】的请求：id={}, brandUpdateDTO={}", id, albumUpdateDTO);
        albumService.updateById(id, albumUpdateDTO);
        return JsonResult.ok();
    }

    // http://localhost:9080/albums/3
    @ApiOperation("根据id查询相册详情")
    @ApiOperationSupport(order = 400)
    @ApiImplicitParam(name = "id", value = "相册id", required = true, dataType = "long")
    @GetMapping("/{id:[0-9]+}")
    public JsonResult<AlbumStandardVO> getStandardById(@PathVariable Long id) {
        log.debug("开始处理【根据id查询相册详情】的请求：id={}", id);
        AlbumStandardVO album = albumService.getStandardById(id);
        return JsonResult.ok(album);
    }

    // http://localhost:9080/albums
    @ApiOperation("查询相册列表")
    @ApiOperationSupport(order = 400)
    @GetMapping("")
    public JsonResult<List<AlbumListItemVO>> list() {
        log.debug("开始处理【查询相册列表】的请求……");
        List<AlbumListItemVO> list = albumService.list();
        return JsonResult.ok(list);
    }

}
