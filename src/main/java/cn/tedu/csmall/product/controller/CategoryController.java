package cn.tedu.csmall.product.controller;

import cn.tedu.csmall.product.pojo.dto.CategoryAddNewDTO;
import cn.tedu.csmall.product.pojo.dto.CategoryUpdateDTO;
import cn.tedu.csmall.product.pojo.vo.CategoryListItemVO;
import cn.tedu.csmall.product.pojo.vo.CategoryStandardVO;
import cn.tedu.csmall.product.service.ICategoryService;
import cn.tedu.csmall.product.web.JsonResult;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 处理类别相关请求的控制器
 *
 * @author java@tedu.cn
 * @version 0.0.1
 */
@Slf4j
@RestController
@RequestMapping("/categories")
@Api(tags = "01. 类别管理模块")
public class CategoryController {

    @Autowired
    private ICategoryService categoryService;

    public CategoryController() {
        log.info("创建控制器：CategoryController");
    }

    // http://localhost:9080/categories/add-new
    @ApiOperation("添加类别")
    @ApiOperationSupport(order = 100)
    @PostMapping("/add-new")
    public JsonResult<Void> addNew(CategoryAddNewDTO categoryAddNewDTO) {
        log.debug("开始处理【添加类别】的请求：{}", categoryAddNewDTO);
        categoryService.addNew(categoryAddNewDTO);
        return JsonResult.ok();
    }

    // http://localhost:9080/categories/9527/delete
    @ApiOperation("删除类别")
    @ApiOperationSupport(order = 200)
    @PostMapping("/{id:[0-9]+}/delete")
    public JsonResult<Void> delete(@PathVariable Long id) {
        log.debug("开始处理【删除类别】的请求：id={}", id);
        categoryService.deleteById(id);
        return JsonResult.ok();
    }

    // http://localhost:9080/categories/3/update
    @ApiOperation("修改类别详情")
    @ApiOperationSupport(order = 300)
    @ApiImplicitParam(name = "id", value = "类别id", required = true, dataType = "long")
    @PostMapping("/{id:[0-9]+}/update")
    public JsonResult<Void> updateById(@PathVariable Long id, CategoryUpdateDTO categoryUpdateDTO) {
        log.debug("开始处理【修改类别详情】的请求：id={}, categoryUpdateDTO={}", id, categoryUpdateDTO);
        categoryService.updateById(id, categoryUpdateDTO);
        return JsonResult.ok();
    }

    // http://localhost:9080/categories/3/enable
    @ApiOperation("启用类别")
    @ApiOperationSupport(order = 310)
    @ApiImplicitParam(name = "id", value = "类别id", required = true, dataType = "long")
    @PostMapping("/{id:[0-9]+}/enable")
    public JsonResult<Void> setEnable(@PathVariable Long id) {
        log.debug("开始处理【启用类别】的请求：id={}", id);
        categoryService.setEnable(id);
        return JsonResult.ok();
    }

    // http://localhost:9080/categories/3/disable
    @ApiOperation("禁用类别")
    @ApiOperationSupport(order = 311)
    @ApiImplicitParam(name = "id", value = "类别id", required = true, dataType = "long")
    @PostMapping("/{id:[0-9]+}/disable")
    public JsonResult<Void> setDisable(@PathVariable Long id) {
        log.debug("开始处理【禁用类别】的请求：id={}", id);
        categoryService.setDisable(id);
        return JsonResult.ok();
    }

    // http://localhost:9080/categories/3/display
    @ApiOperation("显示类别")
    @ApiOperationSupport(order = 312)
    @ApiImplicitParam(name = "id", value = "类别id", required = true, dataType = "long")
    @PostMapping("/{id:[0-9]+}/display")
    public JsonResult<Void> setDisplay(@PathVariable Long id) {
        log.debug("开始处理【显示类别】的请求：id={}", id);
        categoryService.setDisplay(id);
        return JsonResult.ok();
    }

    // http://localhost:9080/categories/3/hidden
    @ApiOperation("隐藏类别")
    @ApiOperationSupport(order = 313)
    @ApiImplicitParam(name = "id", value = "类别id", required = true, dataType = "long")
    @PostMapping("/{id:[0-9]+}/hidden")
    public JsonResult<Void> setHidden(@PathVariable Long id) {
        log.debug("开始处理【隐藏类别】的请求：id={}", id);
        categoryService.setHidden(id);
        return JsonResult.ok();
    }

    // http://localhost:9080/categories/3
    @ApiOperation("根据id查询类别详情")
    @ApiOperationSupport(order = 400)
    @ApiImplicitParam(name = "id", value = "类别id", required = true, dataType = "long")
    @GetMapping("/{id:[0-9]+}")
    public JsonResult<CategoryStandardVO> getStandardById(@PathVariable Long id) {
        log.debug("开始处理【根据id查询类别详情】的请求：id={}", id);
        CategoryStandardVO category = categoryService.getStandardById(id);
        return JsonResult.ok(category);
    }

    // http://localhost:9080/categories/list-by-parent
    @ApiOperation("根据父级类别查询子级类别列表")
    @ApiOperationSupport(order = 410)
    @ApiImplicitParam(name = "parentId", value = "父级类别id，如果是一级类别，则此参数值应该为0",
            required = true, dataType = "long")
    @GetMapping("/list-by-parent")
    public JsonResult<List<CategoryListItemVO>> listByParentId(Long parentId) {
        if (parentId == null || parentId < 0) {
            parentId = 0L;
        }
        List<CategoryListItemVO> list = categoryService.listByParentId(parentId);
        return JsonResult.ok(list);
    }

}
