package cn.tedu.csmall.product.repository;

import cn.tedu.csmall.product.pojo.vo.BrandListItemVO;
import cn.tedu.csmall.product.pojo.vo.BrandStandardVO;

import java.util.List;
import java.util.Set;

/**
 * 处理品牌缓存数据的存储接口
 *
 * @author java@tedu.cn
 * @version 0.0.1
 */
public interface IBrandCacheRepository {

    /**
     * 将某个品牌数据保存到Redis中
     *
     * @param brandStandardVO 品牌数据
     */
    void save(BrandStandardVO brandStandardVO);

    /**
     * 从Redis中取出品牌数据
     *
     * @param id 品牌id
     * @return 匹配的品牌数据，如果Redis中没有匹配的数据，将返回null
     */
    BrandStandardVO get(Long id);

    /**
     * 将品牌列表数据保存到Redis中
     *
     * @param brandList 品牌列表数据
     */
    void saveList(List<BrandListItemVO> brandList);

    /**
     * 从Redis中取出品牌列表数据
     *
     * @return 品牌列表数据
     */
    List<BrandListItemVO> getList();

    /**
     * 获取所有品牌缓存数据的Key
     *
     * @return 所有品牌缓存数据的Key
     */
    Set<String> getAllKeys();

    /**
     * 删除所有缓存的品牌数据
     *
     * @param keys 所有缓存的品牌数据的Key的集合
     * @return 删除的数据的数量
     */
    Long deleteAll(Set<String> keys);

}