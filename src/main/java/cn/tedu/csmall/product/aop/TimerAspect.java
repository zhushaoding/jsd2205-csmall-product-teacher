package cn.tedu.csmall.product.aop;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

@Slf4j
@Aspect
@Component
public class TimerAspect {

    // @Around（环绕）：在匹配到的方法之前和之后都执行
    @Around("execution(* cn.tedu.csmall.product.service.impl.*.*(..))")
    //                 任何返回值类型
    //                                                       任何类
    //                                                         任何方法
    //                                                            无论参数数量多少
    // 除了@Around以外，其实还有：@Before / @After / @AfterReturning / @AfterThrowing
    public Object a(ProceedingJoinPoint pjp) throws Throwable {
        log.debug("TimerAspect执行了切面方法……");
        long start = System.currentTimeMillis();

        // 执行以上@Around注解匹配到的方法
        // 注意：不要try...catch异常
        // 注意：必须获取返回值，并返回
        Object result = pjp.proceed();

        long end = System.currentTimeMillis();
        log.debug("当前业务方法执行耗时：{}毫秒", end - start);

        return result;
    }

}
