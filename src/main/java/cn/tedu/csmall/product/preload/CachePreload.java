package cn.tedu.csmall.product.preload;

import cn.tedu.csmall.product.service.IBrandService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

/**
 * 缓存预热类
 *
 * @author java@tedu.cn
 * @version 0.0.1
 */
@Slf4j
// @Component
public class CachePreload implements ApplicationRunner {

    @Autowired
    private IBrandService brandService;

    public CachePreload() {
        log.debug("创建服务启动后自动执行任务的对象：CachePreload");
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        log.debug("CachePreload.run()");
        brandService.loadBrandsToCache();
        log.debug("缓存预热完成！");
    }

}
